/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.usermanagement;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author User
 */
public class UserService {
    private static ArrayList<User> userList = new ArrayList<>();
    private static User currentUser = null;
    static {
        //Mock Data
        userList.add(new User("admin","password"));
        userList.add(new User("user1","password"));
        userList.add(new User("user2","password"));
    }
    
    public static boolean addUser(User user){
        userList.add(user);
        return true;
    }
    
    public static boolean delUser(User user){
        userList.remove(user);
        return true;
    }     
        
    public static boolean delUser(int index){
        userList.remove(index);
        return true;
    }
    
    public static ArrayList<User> getUser() {
        return userList;
    }
    
    public static User getUser(int index){
        return userList.get(index);
    }
    
    public static boolean updateUser(int index,User user){
        userList.set(index, user);
        return true;
    }
    
    public static void save(){
            File file = null;
            FileOutputStream fos = null;
            ObjectOutput oos = null;
        try {
            file = new File("user.bin");
            fos = new FileOutputStream(file);
            oos = new ObjectOutputStream(fos);
            oos.writeObject(userList);
            oos.close();
            fos.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(UserService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(UserService.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    public static void load(){
            File file = null;
            FileInputStream fis = null;
            ObjectOutput ois = null;
        try {
            file = new File("user.bin");
            fis = new FileOutputStream(file);
            ois = new ObjectOutputStream(fis);
            ois.writeObject(userList);
            ois.close();
            fis.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(UserService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(UserService.class.getName()).log(Level.SEVERE, null, ex);
        }  
    }
    public static User Login(String userName, String password){
        for(int i =0; i < userList.size();i++){
            User user = userList.get(i);
            if(user.getUserName().equals(userName) && user.getPassword().equals(password)) {
                currentUser = user;
                return user;
            } 
        }
        return null;
    }
    
    public static boolean isLoing(){
        return currentUser != null;
    }
    
    public static User getCurrentUser(){
        return currentUser;
    }
    
    public static void logout() {
        currentUser = null;
    }
}
